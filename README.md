# Introduction au types construits

Le but de ce travail est de montrer que l'on peut implémenter une même structure de différentes façons. 
- On montre que l'implémentation est transparente pour l'utilisateur : la même instruction conduit au même résultat 
- La comparaison du temps d'exécution permet de prendre conscience que certaines implémentations sont plus rapides que d'autres dans certains cas.


Pour accéder au notebook, cliquez sur le lien ci-dessous :

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/jose_delamare%2Fnsi-type-abstrait-de-donnees/54e4aad22c32bc6975f1ab769a012d95ffe43ec5?filepath=temps_liste.ipynb)